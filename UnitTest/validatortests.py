import unittest
from urlValidator import url_validator


class ValidatorTest(unittest.TestCase):

    def test_http(self):
        vd = url_validator
        self.assertTrue(vd.url_validate("http://localhost"))


    def test_htps(self):
        vd = url_validator
        self.assertTrue(vd.url_validate("https://localhost"))


    def test_short_http(self):
        vd = url_validator
        self.assertFalse(vd.url_validate("http://"))


    def test_short_https(self):
        vd = url_validator
        self.assertFalse(vd.url_validate("https://"))


    def test_number(self):
        vd = url_validator
        self.assertFalse(vd.url_validate("1234"))


    def test_random_symbol(self):
        vd = url_validator
        self.assertFalse(vd.url_validate("asdfasrf"))
