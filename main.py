# -*- coding: utf8 -*-
import sys
import argparse
from dataprovider import *


def create_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', required=True)
    parser.add_argument('-d', required=True)
    return parser


def main():
    parser = create_parse()
    namespace = parser.parse_args(sys.argv[1:])
    urllist = get_urls(namespace.s)
    if urllist is None: return
    if len(urllist) == 0:
        print("Файл " + namespace.s + " не содержит допустимых URL")
        return
    for url in urllist:
        data = get_data_from_urls(url)
        if data is not None:
            write_data_on_disk(namespace.d, data)
    print("Done")


if __name__ == '__main__':
    main()
