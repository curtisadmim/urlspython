import urllib.request
from urlValidator import url_validator


def get_urls(string):
    try:
        with open(string) as file:
            array = [row.strip() for row in file]
            return array
    except FileNotFoundError:
        print("Файл не найден")
        return None


def get_data_from_urls(link):
    validator = url_validator
    if validator.url_validate(link):
        try:
            response = urllib.request.urlopen(link)
        except urllib.error.URLError:
            print("Строка " + link + " не является валидным URL")
            return None
        html = response.read().decode("utf-8")
        result = html.splitlines()
        return result
    print("Строка " + link + " не является валидным URL")
    return None


def write_data_on_disk(file_path, data):
    try:
        w = open(file_path, 'a')
        w.write("Start Of Url" + '\n')
        for line in data:
            w.write(line + '\n')
        w.write("End Of Url" + '\n')
        w.close()
    except PermissionError:
        print("Нет прав на запись в " + file_path)

